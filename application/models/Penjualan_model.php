<?php defined('BASEPATH') OR exit('No direct script access allowed');

class penjualan_model extends CI_Model
{
    //panggil nama table
    private $_table_header = "penjualan_header";
    private $_table_detail = "penjualan_detail";

    public function tampilDataPenjualan()
    {
        $query	= $this->db->query(
            "SELECT * FROM " . $this->_table_header . " WHERE flag = 1"
        );
        return $query->result();	
    }

    //public function rules()
    //{
       // return [
         //   [
       //     
      //          'field' => 'id_pembelian_h',
        //        'label' => 'No Transaksi',
       //         'rules' => 'required|max_length[5]',
       //         'errors' => [
       //             'required' => 'no transaksi Jenis Tidak Boleh Kosong.',
      //              'max_length'=> 'no transaksi Tidak Boleh Lebih Dari 5 Karakter.',
       //         ],
     //       ],
      //     [
        //        'field' => 'nama_supplier',
          //      'label' => 'Nama Supplier',
            //    'rules' => 'required',
              //  'errors' => [
                //    'required' => 'Nama Supplier Tidak Boleh Kosong.',
                //],
            
          // ],
           // [
             //   'field' => 'nama_barang',
               // 'label' => 'Nama barang',
                //'rules' => 'required',
                //'errors' => [
                  //  'required' => 'Nama barang Tidak Boleh Kosong.',
                //],
            
           // ],
            //[
         //       'field' => 'qty',
        //        'label' => 'qty',
       //         'rules' => 'required|numeric',
       //         'errors' => [
       //             'required' => 'qty Tidak Boleh Kosong.',
       //             'numeric' => 'qty Harus Angka.',
       //        ],
       //     ],
        //    [
       //         'field' => 'harga_barang',
        //        'label' => 'Harga Barang',
        //        'rules' => 'required|numeric',
        //        'errors' => [
        //            'required' => 'Harga Barang Tidak Boleh Kosong.',
        //            'numeric' => 'Harga Barang Harus Angka.',
         //       ],
        //    ]
         //   
       // ];
   // }

    public function savePenjualanHeader()
    {
        $data['no_transaksi']   = $this->input->post('no_transaksi');
        $data['pembeli']        = $this->input->post('pembeli');
        $data['tanggal']        = date('Y-m-d');
        $data['flag']           = 1;

        $this->db->insert($this->_table_header, $data);
    }

    public function idTransaksiTerakhir()
    {
        $query	= $this->db->query(
            "SELECT * FROM " . $this->_table_header . " WHERE flag = 1 ORDER BY id_jual_h DESC LIMIT 0,1"
        );
        $data_id = $query->result();

        foreach ($data_id as $data) {
            $last_id = $data->id_jual_h;
        }

        return $last_id;
    }

    public function tampilDataPenjualanDetail($id)
    {
        $query	= $this->db->query(
            "SELECT A.*, B.nama_barang FROM " . $this->_table_detail . " AS A INNER JOIN barang AS B ON A.kode_barang = B.kode_barang WHERE A.flag = 1 AND A.id_jual_h = '$id'"
        );
        return $query->result();	
    }

    public function savePenjualanDetail($id)
    {
        $qty    = $this->input->post('qty');
        $harga  = $this->input->post('harga');

        $data['id_jual_h'] = $id;
        $data['kode_barang']    = $this->input->post('kode_barang');
        $data['qty']            = $qty;
        $data['harga']          = $harga;
        $data['jumlah']         = $qty * $harga;
        $data['flag']           = 1;

        $this->db->insert($this->_table_detail, $data);
    }

    public function tampilreportpenjualan($tgl_awal,$tgl_akhir)
  {
    $this->db->select('barang.stok, penjualan_detail.qty, penjualan_detail.jumlah,
    pembelian_header.tanggal, pembelian_header.no_transaksi, penjualan_header.id_pembelian_h');
    $this->db->from('pembelian_header');
    $this->db->join('pembelian_detail', 'pembelian_detail.id_jual_h = pembelian_header.id_jual_h');
    $this->db->join('barang', 'pembelian_detail.kode_barang = barang.kode_barang'); 
    
    $this->db->where("pembelian_header.tanggal >=",$tgl_awal);
    $this->db->where("pembelian_header.tanggal <=",$tgl_akhir);
    $this->db->group_by("pembelian_header.no_transaksi", "asc");
    
    $query = $this->db->get();
    return $query->result();
  }

  public function tampilreportpnjualanbaru($tgl_awal,$tgl_akhir)
  {
    //select ph.id_pembelian_h,ph.no_transaksi,ph.tanggal,count(pd.kode_barang)AS kode_barang, SUM(pd.qty),SUM(pd.jumlah)as qty FROM pembelian_header AS ph inner join pembelian_detail AS pd ON ph.id_pembelian_h=pd.id_pembelian_h GROUP BY ph.no_transaksi
    $this->db->select('penjualan_header.id_jual_h,penjualan_header.no_transaksi,penjualan_header.tanggal,penjualan_header.pembeli,sum(penjualan_detail.qty) as qty,sum(penjualan_detail.jumlah)as jumlah,count(barang.stok) as stok, count(barang.kode_barang)');

    $this->db->from('penjualan_header');
    $this->db->join('penjualan_detail', 'penjualan_detail.id_penjualan_h = penjualan_header.id_penjualan_h');
    $this->db->join('barang','penjualan_detail.kode_barang = barang.kode_barang'); 
    
    $this->db->where("penjualan_header.tanggal >=",$tgl_awal);
    $this->db->where("penjualan_header.tanggal <=",$tgl_akhir);
    $this->db->group_by('penjualan_header.no_transaksi');
    
    $query = $this->db->get();
    return $query->result();
  }


  public function createKodeUrutTransaksi() {

    date_default_timezone_set('Asia/Jakarta');
    //cek kode barang terakhir
    $this->db->select('MAX(no_transaksi) as no_transaksi');
    $query = $this->db->get($this->_table_header);
    $result = $query->row_array(); //hasil berbentuk array

    $no_transaksi_terakhir = $result['no_transaksi'];
    //format TR90410A01 = TR(label awal), 9(TAHUN), 04 (BULAN), 11(JAM)
    // A/B -> JIKA JAM GENAP MAKA A 
    //     -> JIKA JAM GANJIL BERARTI B
    //01 (NO URUT)
    $label = "PJ";
    $thn_sekarang=date('y');
    $bln_sekarang=date('m');
    $jam_sekarang=date('H');
    $rubah_jam ="";
    if($jam_sekarang%2==0){
      $rubah_jam="A";//JAM GENAP
    }else{
      $rubah_jam="B";//JAM GANJIL
    }
    $no_urut_lama = (int) substr($no_transaksi_terakhir, 8, 2);
    $no_urut_lama ++;

    $no_urut_baru = sprintf("%02s", $no_urut_lama);
    $no_transaksi_baru = $label .  $thn_sekarang . $bln_sekarang . $jam_sekarang . $rubah_jam . $no_urut_baru;

    //var dump($kode_barang_baru); die();
    //var dump(sprintf("%03s")); die();

    return $no_transaksi_baru;
  }


  
}
