<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan_model extends CI_Model
{
	//panggil nama table
	private $_table = "karyawan";
	
	public function tampilDataKaryawan()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	
	}
	
	public function rules()
	{
		return [
			[
			
				'field' => 'nik',
				'label' => 'NIK',
				'rules' => 'required|max_length[10]',
				'errors' => [
					'required' => 'NIK Tidak Boleh Kosong.',
					'max_length'=> 'NIK Tidak Boleh Lebih Dari 10 Karakter.',
				],
			],
			[
				'field' => 'nama',
				'label' => 'Nama',
				'rules' => 'required',
				'errors' => [
					'required' => 'Nama Tidak Boleh Kosong.',
				],
			],
			[
				'field' => 'tempatlahir',
				'label' => 'Tempat Lahir',
				'rules' => 'required',
				'errors' => [
					'required' => 'Tempat Lahir Tidak Boleh Kosong.',
				],
			],
			//[
			//	'field' => 'tgl_lahir',
			//	'label' => 'Tanggal Lahir',
			//	'rules' => 'required',
			//	'errors' => [
			//		'required' => 'Tanggal Lahir Tidak Boleh Kosong.',
			//	],
			//],
			[
				'field' => 'jenis_kelamin',
				'label' => 'Jenis Kelamin',
				'rules' => 'required',
				'errors' => [
					'required' => 'Jenis Kelamin Tidak Boleh Kosong.',
				],
			],
			[
				'field' => 'alamat',
				'label' => 'Alamat',
				'rules' => 'required',
				'errors' => [
					'required' => 'Alamat Tidak Boleh Kosong.',
				],
			],
			[
				'field' => 'telp',
				'label' => 'Telepon',
				'rules' => 'required',
				'errors' => [
					'required' => 'Telepon Tidak Boleh Kosong.',
				],
			],
			[
				'field' => 'kode_jabatan',
				'label' => 'Jabatan',
				'rules' => 'required',
				'errors' => [
					'required' => 'Kode Jabatan Tidak Boleh Kosong.',
				],
			]
			
		];	
	}

	public function tampilDataKaryawan2()
	{
		$query = $this->db->query("SELECT * FROM karyawan WHERE flag = 1");
		return $query->result();
	
	}
	
	public function tampilDataKaryawan3()
	{
		$this->db->select('*');
		$this->db->order_by('nik', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function save()
	{
		$tgl 			= $this->input->post('tgl');
		$bln 			= $this->input->post('bln');
		$thn 			= $this->input->post('thn');
		$nik_karyawan 	= $this->input->post('nik');
		$tgl_gabung = $thn. "-" . $bln. "-" . $tgl;
		
		$data['nik']				=$nik_karyawan;
		$data['nama_lengkap']		=$this->input->post('nama');
		$data['tempat_lahir']		=$this->input->post('tempatlahir');
		$data['tgl_lahir']			=$tgl_gabung;
		$data['jenis_kelamin']		=$this->input->post('jenis_kelamin');
		$data['alamat']				=$this->input->post('alamat');
		$data['telp']				=$this->input->post('telp');
		$data['kode_jabatan']		=$this->input->post('kode_jabatan');
		$data['foto']				=$this->uploadphoto($nik_karyawan);
		$data['flag']				=1;
		
		//echo "<pre>";
		//print_r($_FILES["image"]); die();
		//echo "</pre>";

		$this->db->insert($this->_table, $data);
	}
	
	public function detail($nik)
	{
		$this->db->select('*');
		$this->db->where('nik', $nik);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function edit($nik)
	{
		$this->db->select('*');
		$this->db->where('nik', $nik);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function update($nik)
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabung = $thn. "-" . $bln. "-" . $tgl;
		
		$data['nama_lengkap']		=$this->input->post('nama');
		$data['tempat_lahir']		=$this->input->post('tempatlahir');
		$data['tgl_lahir']			=$tgl_gabung;
		$data['jenis_kelamin']		=$this->input->post('jenis_kelamin');
		$data['alamat']				=$this->input->post('alamat');
		$data['telp']				=$this->input->post('telp');
		$data['kode_jabatan']		=$this->input->post('kode_jabatan');
		$data['flag']				=1;
		
		if(!empty($_FILES["image"] ["name"])) {
			$this->hapusphoto($nik);
			$foto_karyawan		= $this->uploadphoto($nik);
		} else {
			$foto_karyawan		= $this->input->post('foto_old');
		}

		$data['foto']			= $foto_karyawan;

		$this->db->where('nik', $nik);
		$this->db->update($this->_table, $data);
	}

	public function delete ($nik)
	{
		$this->hapusphoto($nik);
		$this->db->where('nik', $nik);
		$this->db->delete($this->_table);	
	}
	
	private function uploadphoto($nik)
	{
		$date_upload			= date('Ymd');
		$config['upload_path']	= './resources/fotokaryawan';
		$config['allowed_types']= 'gif|jpg|png|jpeg';
		$config['file_name']	= $date_upload . '_' . $nik;
		$config['overwrite']	= true;
		$config['max_size']		= 1024; //1MB
		//$config['max_width']	= 1024;
		//$config['max_height']	= 768;

		//echo "<pre>";
		//print_r($_files["image"]); die();
		//echo "</pre";

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('image')) {
			$nama_file	= $this->upload->data("file_name");
		} else {
			$nama_file	= "default.png";
		}

		return $nama_file;
	}

	private function hapusphoto($nik)
	{
		//cari nama foto
		$data_karyawan = $this->detail($nik);
		foreach ($data_karyawan as $data) {
			$nama_file = $data->foto;
		}

		if ($nama_file != "default.png") {
			$path = "./resources/fotokaryawan" .$nama_file;
			//bentuk path nya : ./resources/fotokaryawan/2017266.987/123.jpeg
			return @unlink($path);
		}
	}

	public function tampilDataKaryawanPagination($perpage, $uri, $data_pencarian)
	{
		$this->db->select('*');
		if (!empty($data_pencarian)) {
			$this->db->like('nama_lengkap', $data_pencarian);
		}
		$this->db->order_by('nik','asc');

		$get_data = $this->db->get($this->_table, $perpage, $uri);
		if ($get_data->num_rows() > 0) {
			return $get_data->result();
		} else {
			return null;
		}
	}

	public function tombolpagination($data_pencarian)
	{
		//cari jumlah data berdasarkan data pencarian
		$this->db->like('nama_lengkap', $data_pencarian);
		$this->db->from($this->_table);
		$hasil = $this->db->count_all_results();

		//pagination limit
		$pagination['base_url'] = base_url().'karyawan/listkaryawan/load/';
		$pagination['total_rows'] = $hasil;
		$pagination['per_page'] = "3";
		$pagination['uri_segment'] = 4;
		$pagination['num_links'] = 2;

		//custom paging configuration

		$pagination['full_tag_open'] = '<div class="pagination">';
		$pagination['full_tag_close'] = '<div>';

		$pagination['first_link'] = 'First Page';
		$pagination['first_tag_open'] = '<span class="fistlink">';
		$pagination['first_tag_close'] = '</span>';

		$pagination['last_link'] = 'Last Page';
		$pagination['last_tag_open'] = '<span class="lastlink">';
		$pagination['last_tag_close'] = '</span>';

		$pagination['next_link'] = 'Next Page';
		$pagination['next_tag_open'] = '<span class="nextlink">';
		$pagination['next_tag_close'] = '</span>';

		$pagination['prev_link'] = 'Prev Page';
		$pagination['prev_tag_open'] = '<span class="prevlink">';
		$pagination['prev_tag_close'] = '</span>';

		$pagination['cur_tag_open'] = '<span class="curlink">';
		$pagination['cur_tag_close'] = '</span>';

		$pagination['num_tag_open'] = '<span class="numlink">';
		$pagination['num_tag_close'] = '</span>';

		$this->pagination->initialize($pagination);

		$hasil_pagination = $this->tampilDataKaryawanPagination($pagination['per_page'],
		$this->uri->segment(4), $data_pencarian);

		return $hasil_pagination;

	}

	public function createKodeUrut() {
		//cek kode barang terakhir
		$this->db->select('MAX(nik) as nik');
		$query = $this->db->get($this->_table);
		$result = $query->row_array(); //hasil berbentuk array

		$nik_terakhir = $result['nik'];
		//format 1901002 = 19(tahun_input), 02(bulan), 001(digit urut)
		$thn_sekarang=date('y');
		$bln_sekarang=date('m');
		$no_urut_lama = (int) substr($nik_terakhir, 4, 3);
		$no_urut_lama ++;

		$no_urut_baru = sprintf("%03s", $no_urut_lama);
		$nik_baru = $thn_sekarang . $bln_sekarang . $no_urut_baru;

		//var dump($kode_barang_baru); die();
		//var dump(sprintf("%03s")); die();

		return $nik_baru;
	}

}
