<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier_model extends CI_Model
{
	//panggil nama table
	private $_table = "supplier";
	
	public function tampilDataSupplier()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	
	}
	
	public function rules()
	{
		return [
			[
			
				'field' => 'kode_supplier',
				'label' => 'Kode supplier',
				'rules' => 'required|max_length[5]',
				'errors' => [
					'required' => 'Kode supplier Tidak Boleh Kosong.',
					'max_length'=> 'Kode supplier Tidak Boleh Lebih Dari 5 Karakter.',
				],
			],
			[
				'field' => 'nama_supplier',
				'label' => 'Nama supplier',
				'rules' => 'required',
				'errors' => [
					'required' => 'Nama Barang Tidak Boleh Kosong.',		
				],
			],
			[
				'field' => 'alamat',
				'label' => 'Alamat',
				'rules' => 'required',
				'errors' => [
					'required' => 'Alamat Tidak Boleh Kosong.',
				],
			],
			[
				'field' => 'telp',
				'label' => 'Telepon',
				'rules' => 'required',
				'errors' => [
					'required' => 'Telepon Tidak Boleh Kosong.',
				],
			]
			
		];	
	}

	public function tampilDataSupplier2()
	{
		$query = $this->db->query("SELECT * FROM supplier WHERE flag = 1");
		return $query->result();
	
	}
	
	public function tampilDataSupplier3()
	{
		$this->db->select('*');
		$this->db->order_by('kode_supplier', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function save()
	{
		$data['kode_supplier']		=$this->input->post('kode_supplier');
		$data['nama_supplier']		=$this->input->post('nama_supplier');
		$data['alamat']				=$this->input->post('alamat');
		$data['telp']				=$this->input->post('telp');
		$data['flag']				=1;
		$this->db->insert($this->_table, $data);
	}
	
	public function detail($kode_supplier)
	{
		$this->db->select('*');
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function edit($kode_supplier)
	{
		$this->db->select('*');
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function update($kode_supplier)
	{
		$data['kode_supplier']		=$this->input->post('kode_supplier');
		$data['nama_supplier']		=$this->input->post('nama_supplier');
		$data['alamat']				=$this->input->post('alamat');
		$data['telp']				=$this->input->post('telp');
		$data['flag']				=1;
		
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->update($this->_table, $data);
	}
	
	public function delete ($kode_supplier)
	{
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->delete($this->_table);	
	}

	public function TampilDataSupplierPagination($perpage, $uri, $data_pencarian)
	{
	$this->db->select('*');
	if (!empty($data_pencarian)) {
		$this->db->like('nama_supplier', $data_pencarian);
	}
	$this->db->order_by('kode_supplier','ASC');

	$get_data = $this->db->get($this->_table, $perpage, $uri);
	if ($get_data->num_rows() > 0) {
		return $get_data->result();
	} else {
		return null;
		}
	}
	
	public function tombolpagination($data_pencarian)
	{
		//cari jumlah data berdasarkan pencarian
		$this->db->like('nama_supplier', $data_pencarian);
		$this->db->from($this->_table);
		$hasil = $this->db->count_all_results();
		
		//pagination limit
		$pagination['base_url'] = base_url().'supplier/listsupplier/load/';
		$pagination['total_rows'] = $hasil;
		$pagination['per_page'] = "3";
		$pagination['uri_segment'] = 4;
		$pagination['num_links'] = 2;
		
		//custom paging configuration
		$pagination['full_tag_open'] = '<div class="pagination">';
		$pagination['full_tag_close'] = '</div>';
		
		$pagination['first_link'] = 'First Page';
		$pagination['first_tag_open'] = '<span class="firstlink">';
		$pagination['first_tag_close'] = '</span>';
		
		$pagination['last_link'] = 'Last Page';
		$pagination['last_tag_open'] = '<span class="lastlink">';
		$pagination['last_tag_close'] = '</span>';
		
		$pagination['next_link'] = 'Next Page';
		$pagination['next_tag_open'] = '<span class="nextlink">';
		$pagination['next_tag_close'] = '</span>';
		
		$pagination['prev_link'] = 'Prev Page';
		$pagination['prev_tag_open'] = '<span class="prevlink">';
		$pagination['prev_tag_close'] = '</span>';
		
		$pagination['cur_tag_open'] = '<span class="curlink">';
		$pagination['cur_tag_close'] = '</span>';
		
		$pagination['num_tag_open'] = '<span class="numlink">';
		$pagination['num_tag_close'] = '</span>';
		
		$this->pagination->initialize($pagination);
		
		$hasil_pagination = $this->tampilDataSupplierPagination($pagination['per_page'],
		$this->uri->segment(4), $data_pencarian);
		
		return $hasil_pagination;
	}


	public function createKodeUrut() {
		//cek kode barang terakhir
		$this->db->select('MAX(kode_supplier) as kode_supplier');
		$query = $this->db->get($this->_table);
		$result = $query->row_array(); //hasil berbentuk array

		$kode_supplier_terakhir = $result['kode_supplier'];
		//format BR001 = BR(label awal), 001(no urut)
		$label = "SP";
		$no_urut_lama = (int) substr($kode_supplier_terakhir, 2, 3);
		$no_urut_lama ++;

		$no_urut_baru = sprintf("%03s", $no_urut_lama);
		$kode_supplier_baru = $label . $no_urut_baru;

		//var dump($kode_barang_baru); die();
		//var dump(sprintf("%03s")); die();

		return $kode_supplier_baru;
	}


}
