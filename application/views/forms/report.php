<head>
  <form method="POST" action="<?=base_url()?>Pembelian/Report_pembelian">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>jQuery UI Datepicker - Default functionality</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker();
    $( "#datepickerakhir" ).datepicker();
  } );
  </script>

  <script type="text/javascript">
  $(document).ready(function(){
  
   $('#Proses').on('click', function(event) {
    event.preventDefault();
    var tgl_awal = $("#datepicker").val();
    var tgl_akhir = $("#datepickerakhir").val();

    if (tgl_awal == "" || tgl_akhir == "") {
      alert('Tanggal Tidak Boleh Kosong');
    } else if (new Date (tgl_awal) > new Date (tgl_akhir)) {
      alert('Format Waktu Salah Input');
    } else{
      $('#forms').submit();
    }
   });
});
 </script>
</head>
<center>
  

<body>
 <tr>
   <p>Tanggal Awal: <input type="text" name="tgl_awal" id="datepicker" class="form-control" style="width:80px;height:25px"></p>
 </tr>
<tr>
  <p>Tanggal Akhir: <input type="text" name="tgl_akhir" id="datepickerakhir" class="form-control" style="width:80px;height:25px"></p>
</tr>
<tr>
  <td><input type="submit" name="submit" id="submit" value="Proses" onClick="return valid();"></td>
</tr>

</body>
</center>
</html>
</form>