<table width="100%" height="54" border="1">
  <tr>
    <td height="23" colspan="6" align="center" bgcolor="#3399FF"><b>TOKO JAYA ABADI</b></td>
  </tr>
  <tr bgcolor="#33CCFF">
    <td height="23" align="center"><li><a href="<?=base_url();?>main">Home</a></li></td>
    <td align="center"><li><a href="<?=base_url();?>karyawan/listkaryawan">Master</a></li></td>
    <td align="center"><li><a href="<?=base_url();?>jabatan/listjabatan">Transaksi</a></li></td>
    <td align="center"><li><a href="<?=base_url();?>barang/listbarang">Report</a></li></td>
    <td align="center"><li><a href="<?=base_url();?>jenis_barang/listjenisbarang">Logout</a></li></td>
  </table>
  
 
<div align="center">
<h1>Input Barang</h1></div>
<div align="center" style="color:red"><?= validation_errors();?></div>
<form action="<?=base_url()?>barang/inputbarang" method="POST">
<table width="50%" border="0" cellspacing="0" cellpadding="5" align="center" >
  <tr>
    <td width="37%">Kode Barang</td>
    <td width="4%"> :</td>
    <td width="59%">
      <input type="text" name="kode_barang" id="kode_barang" value="<?= $data_barang; ?>" readonly="on" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Nama Barang </td>
    <td>:</td>
    <td>
      <input type="text" name="nama_barang" id="nama_barang" value="<?=set_value('nama_barang');?>" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Harga Barang</td>
    <td> :</td>
    <td><input type="text" name="harga_barang" id="harga_barang" value="<?=set_value('harga_barang');?>" maxlength="50" /></td>
  </tr>
  <tr>
    <td>Kode Jenis</td>
    <td>:</td>
    <td>
    <select name="kode_jenis" id="kode_jenis">
      <?php foreach($data_jenis_barang as $data){?>
      <option value="<?= $data->kode_jenis;?>">
        <?= $data->nama_jenis;?>
        </option> 
      <?php }?>
    </select>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
      <input type="submit" name="Submit" id="Submit" value="Simpan">
      <input type="reset" name="Batal" id="Batal" value="Batal">
      </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
    	<a href="<?=base_url();?>barang/listbarang">
      <input type="button" name="Submit" id="Submit" value="Kembali Ke Menu Sebelumnya"> </a>
    </td>
  </tr>
</table>
</form>
