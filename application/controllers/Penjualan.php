<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("penjualan_model");
		$this->load->model("barang_model");

        //$this->load->library('form_validation');
	}

	public function index()
	{
		$this->listPenjualan();
    }
    
    public function listPenjualan()
	{
		$data['data_penjualan']     = $this->penjualan_model->tampilDataPenjualan();
        $data['content'] 		    ='forms/input_penjualan';      
        $data['content']            ='forms/input_penjualan_detail';

		$this->load->view('home_', $data);
    }
    
    public function input_h()
	{
        // panggil data supplier untuk kebutuhan form input
        //$data['data_supplier'] 	    = $this->supplier_model->tampilDataSupplier();

         $data['nomor_transaksi']   = $this->penjualan_model->createKodeUrutTransaksi();
        
        // proses simpan ke pembelian header jika ada request form
        if (!empty($_REQUEST)) {
            $penjualan_header = $this->penjualan_model;
            $penjualan_header->savePenjualanHeader();
            
            //panggil ID transaksi terakhir
            $id_terakhir = $penjualan_header->idTransaksiTerakhir();
            // var_dump($id_terakhir); die();
            
            // redirect("pembelian/index", "refresh");
            // redirect ke halaman input pembelian detail
		      redirect("penjualan/input_d/" . $id_terakhir, "refresh");
            }
                
                
               $data['content']    ='forms/input_penjualan';
                $this->load->view('home_', $data);

        
		//$this->load->view('input_pembelian', $data);
    }
    
    public function input_d($id_penjualan_header)
	{
        // panggil data barang untuk kebutuhan form input
        $data['data_barang'] 	= $this->barang_model->tampilDataBarang();
        $data['id_header'] 	    = $id_penjualan_header;
        $data['data_penjualan_detail'] 	= $this->penjualan_model->tampilDataPenjualanDetail($id_penjualan_header);
        
        // proses simpan ke pembelian detail jika ada request form
        if (!empty($_REQUEST)) {
            //save detail
            $this->penjualan_model->savePenjualanDetail($id_penjualan_header);
            
            //proses update stok
            $kd_barang  = $this->input->post('kode_barang');
            $qty        = $this->input->post('qty');
            $this->barang_model->updateStokPenjualan($kd_barang, $qty);
			redirect("penjualan/input_d/" . $id_penjualan_header, "refresh");
		}
        
		$data['content'] 		= 'forms/input_penjualan_detail';
		//$this->load->view('input_pembelian_detail', $data);
        $this->load->view('home_', $data);
    }

    public function report() {
        $data['content'] = 'forms/report';
        $this->load->view('home_', $data);
    }
    
    public function report_penjualan() {

        $tgl_awal=$this->input->post('tgl_awal');
        $pisah=explode('/',$tgl_awal);
        $array=array($pisah[2],$pisah[0],$pisah[1]);
        $tgl_awal=implode('-',$array);
        
        $tgl_akhir=$this->input->post('tgl_akhir');
        $pisah=explode('/',$tgl_akhir);
        $array=array($pisah[2],$pisah[0],$pisah[1]);
        $tgl_akhir=implode('-',$array);
        
        $data['tgl_awal']=$this->input->post('tgl_awal');
        $data['tgl_akhir']=$this->input->post('tgl_akhir');
       // $data['data_pembelian'] = $this->pembelian_model->tampilreportpembelian($tgl_awal,$tgl_akhir);
        $data['data_pembelian'] = $this->penjualan_model->tampilreportpenjualanbaru($tgl_awal,$tgl_akhir);
        $data['content'] = 'forms/report_penjualan';
        $this->load->view('home_', $data);      
    }

    public function laporan()
    {
        $data['content'] = 'forms/report_penjualan';
        $this->load->view('home_', $data);   
    }

    public function carilaporan()
    {
        if (!empty($_REQUEST)) {
            //ambil proses tanggal
            $tgl_awal   =$this->input->post('tgl_awal');
            $tgl_akhir  =$this->input->post('tgl_akhir');
            $data['data_cari_penjualan'] = $this->penjualan_model->tampilreportpenjualanbaru($tgl_awal,$tgl_akhir);
            $data['tgl_awal']    =$tgl_awal;
            $data['tgl_akhir']   =$tgl_akhir;

            $data['content'] = 'forms/report_penjualan';
            $this->load->view('home_', $data);   
        } else {
            redirect("penjualan/laporan", "refresh");
        }
    }
	
}