<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("barang_model");
		$this->load->model("jenis_barang_model");
		
		$this->load->library('form_validation');
		
	}
	
	public function index()
	{
		$this->listbarang();
	}
	
	public function listbarang()
	{
		if (isset($_POST['tombol_cari'])) {
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('session_pencarian', $data['kata_pencarian']);
		} else {
			$data['kata_pencarian'] = $this->session->userdata('session_pencarian');
		}

		$data['data_barang'] = $this->barang_model->tombolpagination($data['kata_pencarian']);
		//$data['data_barang'] = $this->barang_model->tampilDataBarang();
		$data['content'] = 'forms/list_barang';
		$this->load->view('home_', $data);
	}
	
	public function inputbarang()
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDataJenisBarang();
		$data['content'] ='forms/input_barang';
		$data['data_barang'] = $this->barang_model->createKodeUrut();

		//if(!empty($_REQUEST)) {
		//	$m_barang = $this->barang_model;
		//	$m_barang->save();
		//redirect("barang/index", "refresh");
		//	}
		
		//validasi terlebih dahulu
		$validation = $this->form_validation;
		$validation->set_rules($this->barang_model->rules());
		
		if ($validation->run()){
			$this->barang_model->save();
			$this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil</div>');
			redirect("barang/index", "refresh");
			}
			
		$this->load->view('home_', $data);
	}
	
	public function detailbarang($kode_barang)
	{
		$data['detail_barang'] = $this->barang_model->detail($kode_barang);
		$this->load->view('detail_barang', $data);
	}
	
	public function editbarang($kode_barang)
	{
		$data['data_jenis_barang']	= $this->jenis_barang_model->tampilDataJenisBarang();
		$data['edit_barang'] 		= $this->barang_model->tampilDataBarang();
		
		//if(!empty($_REQUEST)) {
		//$m_barang = $this->barang_model;
		//$m_barang->update($kode_barang);
		//redirect("barang/index", "refresh");
		//}
		
		$validation = $this->form_validation;
		$validation->set_rules($this->barang_model->rules());
		
		if ($validation->run()){
			$this->barang_model->update($kode_barang);
			$this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil</div>');
			redirect("barang/index", "refresh");
			}
			
		$data['content'] ='forms/edit_barang';
		$this->load->view('home_', $data);	
	}
	
	public function deletebarang($kode_barang)
	{
			$m_barang = $this->barang_model;
			$m_barang->delete($kode_barang);
			redirect("barang/index", "refresh");
	}
	
	
}